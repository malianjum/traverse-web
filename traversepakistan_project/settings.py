import os
gettext = lambda s: s
DATA_DIR = os.path.dirname(os.path.dirname(__file__))
"""
Django settings for traversepakistan_project project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '3mct&&4(gc!v5f5)79q3eq(n81$fd*!1(0$9pf#4kjsow049gw'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = False


ALLOWED_HOSTS = ['*']


# Application definition

ROOT_URLCONF = 'traversepakistan_project.urls'

WSGI_APPLICATION = 'traversepakistan_project.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases



# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en'

TIME_ZONE = 'Asia/Karachi'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(DATA_DIR, 'traversepakistan_project', 'media')
STATIC_ROOT = os.path.join(DATA_DIR, 'static')
# STATIC_ROOT = '/var/www/traversepakistan.com/static/'
STATIC_PATH = os.path.join(BASE_DIR, 'static')

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'traversepakistan_project', 'static'),
   # STATIC_PATH,
)
SITE_ID = 1

TEMPLATE_LOADERS = (

    #for blogs
    'aldryn_boilerplates.template_loaders.AppDirectoriesLoader',
    #for blogs
    'django.template.loaders.app_directories.Loader',
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.eggs.Loader',

)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.contrib.messages.context_processors.messages',
    'django.core.context_processors.i18n',
    'django.core.context_processors.debug',
    'django.core.context_processors.request',
    'django.core.context_processors.media',
    'django.core.context_processors.csrf',
    'django.core.context_processors.tz',
    'sekizai.context_processors.sekizai',
    'django.core.context_processors.static',
    'cms.context_processors.cms_settings',
    #for blogs
    'aldryn_boilerplates.context_processors.boilerplate',
    #for blogs
)
TEMPLATE_PATH = BASE_DIR + '/templates/'
#TEMPLATE_PATH = '/home/traverseuser/traversepakistan/traversepakistan_project/templates/'
TEMPLATE_DIRS = (
    TEMPLATE_PATH,
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'cms.middleware.user.CurrentUserMiddleware',
    'cms.middleware.page.CurrentPageMiddleware',
    'cms.middleware.toolbar.ToolbarMiddleware',
    'cms.middleware.language.LanguageCookieMiddleware',
    'djangocms_disqus.middleware.DisqusMiddleware',
)

INSTALLED_APPS = (
    'djangocms_admin_style',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.admin',
    'django.contrib.sites',
    'django.contrib.sitemaps',
    'django.contrib.staticfiles',
    'django.contrib.messages',
    'cms',
    'analytical',
    'menus',
    'sekizai',
    'mptt',
    'djangocms_text_ckeditor',
    'djangocms_style',
    'djangocms_column',
    'djangocms_file',
    'djangocms_flash',
    'djangocms_googlemap',
    'djangocms_inherit',
    'djangocms_link',
    'djangocms_picture',
    'djangocms_teaser',
    'djangocms_video',
    'reversion',
    'traversepakistan_project',
    'places',
    'geoposition',
    'taggit',
   # 'sorl.thumbnail',
    # for blogs
    'aldryn_apphooks_config',
    'aldryn_boilerplates',
    'aldryn_categories',
    'aldryn_newsblog',
    'aldryn_common',
    'parler',
    'sortedm2m',
    'aldryn_people',
    'aldryn_reversion',
    'filer',
    'hvad',
    'easy_thumbnails',

    'connected_accounts',
    'connected_accounts.providers',
    'djangocms_disqus',
    # for blogs
    'django_social_share',
    #registration
    'social_auth',
    #registration
    'users',
    'djangoratings',
)

GOOGLE_ANALYTICS_PROPERTY_ID = 'UA-68395969-1'

CONNECTED_ACCOUNTS_DISQUS_CONSUMER_KEY = 'ReFW87kbm1zjPZSvsonHCbXcqchlHQfGVCh7TWsJgGaPtP0k1UXgZRbV6xkywV9i'
CONNECTED_ACCOUNTS_DISQUS_CONSUMER_SECRET = 'kLjyYc4SgELpd1pNhcEjxUGOgjoVgSWRfkvFI8MdqaDDOs9JyzMTb69b0VLVOAbt'

#registration
AUTHENTICATION_BACKENDS = (
#    'social_auth.backends.twitter.TwitterBackend',
    'social_auth.backends.facebook.FacebookBackend',
#    'social_auth.backends.google.GoogleOAuthBackend',
#    'social_auth.backends.google.GoogleOAuth2Backend',
#    'social_auth.backends.google.GoogleBackend',
    'django.contrib.auth.backends.ModelBackend',
)

FACEBOOK_APP_ID              = '738066149622840'
FACEBOOK_API_SECRET          = '6b31fe8d38699f979e0ed93d5802bd1d'

FACEBOOK_EXTENDED_PERMISSIONS = ['email']
FACEBOOK_PROFILE_EXTRA_PARAMS = {'locale': 'ru_RU'}
LOGIN_URL = '/login/'
LOGIN_REDIRECT_URL = '/en/blog/people/'
LOGIN_ERROR_URL = '/login-error/'

AUTH_PROFILE_MODULE= 'users.UserProfile'
SESSION_SERIALIZER='django.contrib.sessions.serializers.PickleSerializer'
#registration


LANGUAGES = (
    ## Customize this
    ('en', gettext('en')),
)

CMS_LANGUAGES = {
    ## Customize this
    'default': {
        'public': True,
        'hide_untranslated': False,
        'redirect_on_fallback': True,
    },
    1: [
        {
            'public': True,
            'code': 'en',
            'hide_untranslated': False,
            'name': gettext('en'),
            'redirect_on_fallback': True,
        },
    ],
}

TEMPLATE_PATH = BASE_DIR + '/templates/'
#TEMPLATE_PATH = '/home/traverseuser/traversepakistan/traversepakistan_project/templates/'

CMS_TEMPLATES = (
    ## Customize this
    ('page.html', 'Page'),
    ('about.html', 'About'),
    ('simplePage.html', 'Simple Page'),
)

CMS_PERMISSION = True

CMS_PLACEHOLDER_CONF = {}

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'HOST': 'localhost',
        'NAME': 'traversepakistan',
        'PASSWORD': 'traverse@1234',
        'PORT': '',
        'USER': 'traverseuser'
    }
}

MIGRATION_MODULES = {
    'djangocms_flash': 'djangocms_flash.migrations_django',
    'djangocms_file': 'djangocms_file.migrations_django',
    'djangocms_inherit': 'djangocms_inherit.migrations_django',
    'djangocms_column': 'djangocms_column.migrations_django',
    'djangocms_video': 'djangocms_video.migrations_django',
    'menus': 'menus.migrations_django',
    'djangocms_picture': 'djangocms_picture.migrations_django',
    'djangocms_googlemap': 'djangocms_googlemap.migrations_django',
    'djangocms_style': 'djangocms_style.migrations_django',
    'cms': 'cms.migrations_django',
    'djangocms_teaser': 'djangocms_teaser.migrations_django',
}

# for blogs
THUMBNAIL_PROCESSORS = (
    'easy_thumbnails.processors.colorspace',
    'easy_thumbnails.processors.autocrop',
#     'easy_thumbnails.processors.scale_and_crop',
    'filer.thumbnail_processors.scale_and_crop_with_subject_location',
    'easy_thumbnails.processors.filters',
    'easy_thumbnails.processors.background',
)

STATICFILES_FINDERS = [
    # important! place right before django.contrib.staticfiles.finders.AppDirectoriesFinder
    'aldryn_boilerplates.staticfile_finders.AppDirectoriesFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'django.contrib.staticfiles.finders.FileSystemFinder',
]
#ALDRYN_BOILERPLATE_NAME='bootstrap3'
ALDRYN_BOILERPLATE_NAME='bootstrap3'
THUMBNAIL_ALIASES = {
    '': {
        'small': {'size': (250, 250), 'crop': True},
    },
}
THUMBNAIL_DEBUG = False
SOUTH_MIGRATION_MODULES = {
    'easy_thumbnails': 'easy_thumbnails.south_migrations',
}
THUMBNAIL_HIGH_RESOLUTION = True
THUMBNAIL_MEDIA_ROOT = MEDIA_ROOT + '/thumbnail/'
THUMBNAIL_MEDIA_URL = '/media/thumbnail/'
# for blogs

CKEDITOR_SETTINGS = {
    'language': '{{ language }}',
    'toolbar_CMS': [
        ['Undo', 'Redo'],
        ['cmsplugins', '-', 'ShowBlocks'],
        ['Format', 'Styles'],
    ],
    'skin': 'moono',
}
