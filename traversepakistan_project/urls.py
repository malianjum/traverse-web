# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function, unicode_literals

from cms.sitemaps import CMSSitemap
from django.conf import settings
from django.http import HttpResponse
from django.conf.urls import *  # NOQA
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
import views


admin.autodiscover()

urlpatterns = i18n_patterns('',
    url(r'^admin/', include(admin.site.urls)),  # NOQA
    url(r'^robots.txt$', lambda r: HttpResponse("User-agent: *\n Disallow: /cislamabad")),
    url(r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap',
        {'sitemaps': {'cmspages': CMSSitemap}}),
    url(r'^select2/', include('django_select2.urls')),
    url(r'^places/', include('places.urls',namespace='places')),
    url(r'^users/', include('users.urls',namespace='users')),
    url(r'^about/', views.about, name='about'), 
    url(r'^logout_view/', views.logout_view, name='logout'), 
    url(r'^$', views.index, name='home'),
    url(r'', include('social_auth.urls')),
    url(r'^', include('cms.urls')),
#    url(r'^$', views.index, name='home'),
#    url(r'^about/$', views.about, name='about'),
    
)

# This is only needed when using runserver.
if settings.DEBUG:
    urlpatterns = patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve',  # NOQA
            {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
        ) + staticfiles_urlpatterns() + urlpatterns  # NOQA
