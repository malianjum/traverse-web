from django.shortcuts import render
from places.models import Category, Region, Place
from django.contrib.auth import logout
from django.http import HttpResponseRedirect

def about(request):
    return render(request, 'about.html')

def index(request):
    places = list(Place.objects.filter(featured=True))
    context_dict = {
        'categories': Category.objects.all(),
        'regions': Region.objects.all(),
        'featured_places': places
    }
    return render(request, 'home.html', context_dict)

def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/')