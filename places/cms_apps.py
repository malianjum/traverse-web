from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _


class PlacesApp(CMSApp):
    name = _("Place App")  # give your app a name, this is required
    urls = ["places.urls"]  # link your app to url configuration(s)
    app_name = "places"
    render_template = "<div></div>"

apphook_pool.register(PlacesApp)  # register your app