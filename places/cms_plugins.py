from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
#from polls_plugin.models import PlacesPlugin
from django.utils.translation import ugettext as _


class CMSPlacesPlugin(CMSPluginBase):
#    model = PlacesPlugin  # model where plugin data are saved
    module = _("Places")
    name = _("Places Plugin")  # name of the plugin in the interface
    render_template = True

    def render(self, context, instance, placeholder):
        context.update({'instance': instance})
        return context

plugin_pool.register_plugin(CMSPlacesPlugin)  # register the plugin