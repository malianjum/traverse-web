from cms.models import CMSPlugin
from django.db import models
from geoposition.fields import GeopositionField
from taggit.managers import TaggableManager
from djangoratings.fields import RatingField


class Region(models.Model):
    name = models.CharField(max_length=128, unique=True)
    title = models.CharField(max_length=128)

    def __unicode__(self):
        return self.name


class Category(models.Model):
    name = models.CharField(max_length=128, unique=True)
    title = models.CharField(max_length=128)

    def __unicode__(self):
        return self.name


class Place(models.Model):
    category = models.ForeignKey(Category)
    region = models.ForeignKey(Region)
    name = models.CharField(max_length=128, unique=True)
    title = models.CharField(max_length=128)
    subtitle = models.CharField(max_length=256)
    coverImage = models.CharField(max_length=256)
    description = models.TextField()
    gettingThere = models.TextField()
    featured = models.BooleanField()
    location = GeopositionField()
    tags = TaggableManager()
#    rating = RatingField(range=5, can_change_vote = True)

    def __unicode__(self):
        return self.name
